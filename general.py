import os
import sys
import time
import logging
import tempfile
import traceback

from termcolor import cprint
from logging.handlers import RotatingFileHandler


class Video:
    id = int()
    link = str()
    filename = str()


class Page:
    id = int()
    videos = list()
    content = str()
    filename = str()
    site_name = str()


def timer(foo):
    def wrapper(*args, **kwargs):
        start = time.time()
        foo(*args, **kwargs)
        Writer.my_print(
            msg=Messages.spent_time.format(time=time.time()-start),
            msg_type=MessageTypes.warning
        )
    return wrapper


def check2desu(foo):
    def wrapper(*args, **kwargs):
        resp = Response()
        try:
            tmp = foo(*args, **kwargs)
            resp.success = True
            resp.result = tmp
        except Exception as exc:
            resp.success = False
            resp.message = exc.args[0]
        return resp
    return wrapper


class General:
    windows_log_path = "C:\\Users\\{current_user}\\AppData\\"
    linux_log_path = '/var/log/'
    basedir = os.path.abspath(os.path.dirname(__file__))
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
    }
    pages_upload_folder = os.sep.join([
        basedir, 'uploads', 'pages'])
    videos_upload_folder = os.sep.join([
        basedir, 'uploads', 'videos'])
    if not os.path.exists(pages_upload_folder):
        os.makedirs(pages_upload_folder)
    if not os.path.exists(videos_upload_folder):
        os.makedirs(videos_upload_folder)
    urls = [
        'https://gidonline.io/film/avatar-legenda-ob-aange/',
    ]
    page_id = 1
    video_id = 1


class ProjectBIO:
    name = 'test-case'
    version = '1.0.0'
    description = 'Тестовое задание'


class Messages:
    complete = '... complete'
    can_not_save = 'Can not upload video: {url}'
    spent_time = 'Total time spent: {time:0.2f} s.'
    attempt_to_get_pages = 'Attempting to get pages'
    total_videos = 'Total videos to upload: {count}'
    separator = '====== [program started now] ======'
    attempt_to_save = 'Attempting to save page: {page_name}'
    attempt_to_get_page = 'Attempting to get page from: {url}'
    attempt_to_save_page = 'Attempting to save page {page_name}'
    attempt_to_save_video = 'Attempting to save video from: {url}'
    attempt_to_get_videos = 'Attempting to get videos from page: {page_name}'
    attempt_to_find_videos = 'Attempting to find videos on page: {page_name}'


class Warnings:
    wrng = '[System Warning]'


class Errors:
    err = '[System Error] '
    can_not_create_logger = err + 'Не могу создать логгер'
    wrong_params = err + 'Некорректно переданы аргументы запуска'
    smth_went_wrong = err + 'Что-то пошло не так.'


class MessageTypes:
    info = 'info'
    warning = 'warning'
    critical = 'critical'


class Writer(object):
    """
    Вспомогательный класс для взаимодействия с пользователем.
    Пишет сообщения в консоль и лог-файл.
    """
    def __new__(cls):
        """
        Обеспечивает работу класса в режиме синглтона.
        """
        if not hasattr(cls, 'instance'):
            cls.instance = super(Writer, cls).__new__(cls)
            cls.logger = Logger().get_logger()
        return cls.instance

    @classmethod
    def my_print(cls, msg, msg_type=''):
        """
        Метод, который принимает на вход сообщение,
            после чего пишет его в лог файл и выводит в консоль
        :param msg: Сообщение, которое будет записано в лог-файл
                    и выведено в консоль.
        :param msg_type: Тип сообщения, влияет на форматирование
                         выводимого в консоль сообщения
        """
        if msg_type == MessageTypes.info:
            cprint(msg, color='green')
            cls.logger.info('%s', msg)
        elif msg_type == MessageTypes.warning:
            cprint(msg, color='yellow')
            cls.logger.warning('%s', msg)
        elif msg_type == MessageTypes.critical:
            cprint(msg, color='red')
            cls.logger.critical('%s', msg)
        else:
            cprint(msg)
            cls.logger.info('%s', msg)


class Response():
    """
    Вспомогательный класс для обеспечения удобного взаимодействия между
        функциями. Своего рода API, но на уровне разработчика.
    :param success: булевая переменная.
                    True - если все хорошо.
                    False - если произошла ошибка.
    :param result: Результат взаимодействия.
        Используется, если должны передаваться какие-то данные.
    :param message: Сопутствующее сообщение к результату выполнения.
        О возникшей ошибке будет написано в этой переменной.
    """

    def __init__(self):
        self.success = False
        self.result = None
        self.message = str()


class Logger:
    def __init__(self):
        self.log_filename = ProjectBIO.name + ".log"
        tmp = self.__create_logger(__name__)
        if not tmp.success:
            raise Exception(
                Errors.can_not_create_logger)
        self.logger = tmp.result
        sys.excepthook = self.log_uncaught_exceptions

    @check2desu
    def __create_logger(self, name):
        """
        Создание логгера
        :param name:
        :return:
        """
        hndlr = self.get_handler()
        lg = logging.getLogger(name)
        lg.setLevel(logging.INFO)
        lg.addHandler(hndlr)
        lg.info(Messages.separator)
        return lg

    def get_logger(self):
        return self.logger

    def get_log_format(self):
        """
        Get метод для формата лога
        :return: format
        """
        return logging.Formatter(
            u"%(levelname)-8s - %(name)-22s" +
            "[LINE:%(lineno)5d] - %(asctime)s - %(message)s")

    def get_handler(self):
        """
        Метод для формирования handler для логирования
        :return: handler
        """
        hndlr = RotatingFileHandler(
            self.get_log_file(),
            maxBytes=5 * 1024 * 1024,
            backupCount=2,
            delay=0)
        hndlr.setFormatter(self.get_log_format())
        hndlr.setLevel(logging.INFO)

        return hndlr

    def get_log_dir(self, system):
        """
        Метод возвращает директорию для логов в зависимости от ОС
        :param system: ОС
        :return: директория для логов
        """
        # определение директории для логов
        log_dir = General.linux_log_path
        if not ('linux' in system or 'darwin' in system):
            log_dir = General.windows_log_path.format(
                current_user=os.getlogin())

        # создание директории для логов, если её нет
        if not os.path.exists(log_dir):
            try:
                os.makedirs(log_dir)
            except PermissionError:
                print("Ошибка создания каталога {log_dir}".format(
                    log_dir=log_dir))
                return tempfile.gettempdir()

        return log_dir

    def get_log_file(self):
        """
        Метод для создания logger
        :return: log_file
        """
        log_path = self.get_log_dir(sys.platform)

        # проверка существования каталога и прав записи
        if os.access(log_path, os.W_OK):
            log_file = os.path.join(log_path, self.log_filename)
        else:
            filepath = os.path.join(tempfile.gettempdir(), self.log_filename)
            log_file = filepath
            cprint(f"Лог-файл будет создан во временной директории: {filepath}",
                   color='yellow')

        return log_file

    def log_uncaught_exceptions(
            self, exception_type, exception_msg, traceback_msg):
        """
        Метод для логирования необработанных исключений и завершения программы
        :param exception_type: Тип вызванного исключения
        :param exception_msg: Исключение (текст)
        :param traceback_msg: Ошибки построчно
        :return: -
        """
        self.logger.critical(
            "Некорректное завершение программы. Необработанное исключение "
            "{error_type}:\n  {error_msg}: \n{traceback_msg}".
            format(error_type=exception_type.__name__,
                   error_msg=exception_msg,
                   traceback_msg="".join(traceback.format_tb(traceback_msg))))
        sys.exit()
