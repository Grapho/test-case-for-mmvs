import os
import re
import json
import urllib
import requests
import argparse
import urllib.request

from general import Page
from general import Video
from general import timer
from general import Writer
from general import General
from general import Messages
from general import check2desu
from general import MessageTypes

from bs4 import BeautifulSoup as BS
from urllib.parse import urlsplit


@check2desu
def save_page(page):
    Writer().my_print(
        msg=Messages.attempt_to_save_page.format(page_name=page.filename),
    )
    path = os.sep.join([
        General.pages_upload_folder,
        page.filename])
    with open(path, 'w') as output_file:
        output_file.write(page.content)


@check2desu
def get_videos(page):
    Writer().my_print(
        msg=Messages.attempt_to_get_videos.format(page_name=page.filename),
    )


@check2desu
def save_videos(videos):
    Writer().my_print(
        msg=Messages.total_videos.format(count=len(videos)),
        msg_type=MessageTypes.warning
    )
    for video in videos:
        Writer().my_print(
            msg=Messages.attempt_to_save_video.format(url=video.link),
        )
        try:
            path = os.sep.join([
                General.videos_upload_folder,
                video.filename])
            urllib.request.urlretrieve(video.link, path)
        except:
            Writer().my_print(
                msg=Messages.can_not_save.format(url=video.link),
                msg_type=MessageTypes.critical
            )


@check2desu
def get_pages(links):
    Writer().my_print(
        msg=Messages.attempt_to_get_pages,
    )
    pages = list()
    for url in links:
        Writer().my_print(
            msg=Messages.attempt_to_get_page.format(url=url),
        )
        r = requests.get(url, headers=General.headers)
        page = Page()
        page.id = General.page_id
        page.content = r.content.decode('UTF-8')
        page.site_name = urlsplit(url).netloc.replace('.', '-')
        page.filename = page.site_name + f'-{page.id}.html'
        check = save_page(page)
        if not check.success:
            raise Exception(check.message)
        check = find_videos_on_page(page)
        if not check.success:
            raise Exception(check.message)
        check = save_videos(check.result)
        if not check.success:
            raise Exception(check.message)

        pages.append(page)
        General.page_id += 1
        General.video_id = 1
        Writer().my_print(
            msg=Messages.complete,
            msg_type=MessageTypes.info
        )

    return pages


@check2desu
def find_videos_on_page(page):
    videos = list()
    Writer().my_print(
        msg=Messages.attempt_to_find_videos.format(page_name=page.filename),
    )
    video_links = [x.group()
                   for x in re.finditer(r'http(.*?)\.mp4', page.content)]

    soup = BS(page.content, 'html5lib')
    frames = soup.findAll("iframe")

    check = get_pages([i.get('src') for i in frames])
    if not check.success:
        raise Exception(check.message)
    contents = [t.content for t in check.result]

    soup = BS(page.content, 'html5lib')
    video_frames = soup.findAll("video")
    contents += [i.get('src') for i in video_frames]

    for sub in contents:
        links = [x.group() for x in re.finditer(r'http(.*?).mp4', sub)]
        for link in links:
            if link not in video_links:
                video_links.append(link)
    for link in video_links:
        video = Video()
        video.id = General.video_id
        video.link = link
        video.filename = f'video-{page.site_name}-{video.id}.mp4'
        videos.append(video)
        General.video_id += 1

    return videos


@check2desu
@timer
def main():
    check = get_pages(General.urls)
    if not check.success:
        raise Exception(check.message)


if __name__ == '__main__':
    check = main()
    if not check.success:
        Writer().my_print(
            msg=check.message,
            msg_type=MessageTypes.critical
        )
    else:
        Writer().my_print(
            msg='ok',
            msg_type=MessageTypes.info
        )
